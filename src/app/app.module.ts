import { UsersService } from './users/users.service';
import {RouterModule} from '@angular/router';
import {HttpModule} from '@angular/http';
import { MessagesService } from './messages/messages.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { MessagesComponent } from './messages/messages.component';
import { MessagesFormComponent } from './messages/messages-form/messages-form.component';
import { FormsModule, ReactiveFormsModule} from "@angular/forms";
import { NavigationComponent } from './navigation/navigation.component';
import { UsersComponent } from './users/users.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { MessageComponent } from './messages/message/message.component';
import { MessagesUpdateFormComponent } from './messages/messages-update-form/messages-update-form.component';

@NgModule({
  declarations: [
    AppComponent,
    MessagesComponent,
    MessagesFormComponent,
    NavigationComponent,
    UsersComponent,
    NotFoundComponent,
    MessageComponent,
    MessagesUpdateFormComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot([
      {pathMatch:'full', path:'', component:MessagesComponent},
      {pathMatch:'full', path:'users', component:UsersComponent},
      {pathMatch:'full', path:'message/:id', component:MessageComponent},
      {pathMatch:'full', path:'messages-update-form/:id', component:MessagesUpdateFormComponent},
      {pathMatch:'full', path:'**', component:NotFoundComponent}//have to be last
    ])
  ],
  providers: [
    MessagesService,
    UsersService
    ],
  bootstrap: [AppComponent]
})
export class AppModule { }