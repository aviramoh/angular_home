import { Injectable } from '@angular/core';
import {Http, Headers} from '@angular/http';
import {HttpParams} from '@angular/common/http';


@Injectable()
export class UsersService {
  http:Http;

  getUsers(){
    return this.http.get('http://localhost:10080/slim/users'); 
  }
  constructor(http:Http) { 
    this.http = http;
  }

}
