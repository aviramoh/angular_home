import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl } from '@angular/forms';
import { MessagesService } from './../messages.service';
import { Component, OnInit, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'messages-update-form',
  templateUrl: './messages-update-form.component.html',
  styleUrls: ['./messages-update-form.component.css']
})
export class MessagesUpdateFormComponent implements OnInit {
  
  @Output() addMessage:EventEmitter<any> = new EventEmitter<any>(); //any is the type. could be [] or {}
  @Output() addMessagePs:EventEmitter<any> = new EventEmitter<any>(); //pessimistic


  service:MessagesService;
  msgform = new FormGroup({
    message:new FormControl(),
    user:new FormControl(),
  });

  constructor(private route: ActivatedRoute ,service: MessagesService, private router: Router) {
    this.service = service;
  }

  sendData() {
    //הפליטה של העדכון לאב
  this.addMessage.emit(this.msgform.value.message);
  console.log(this.msgform.value);
  this.route.paramMap.subscribe(params=>{
    let id = params.get('id');
    this.service.putMessage(this.msgform.value, id).subscribe(
      response => {
        console.log(response.json());
        this.addMessagePs.emit();
        this.router.navigateByUrl("/");//return to main page
      }
    );
  })
}
  
  message;
  ngOnInit() {
    this.route.paramMap.subscribe(params=>{
      let id = params.get('id');
      console.log(id);
      this.service.getMessage(id).subscribe(response=>{
        this.message = response.json();
        console.log(this.message);
      })
    })
  }

}
