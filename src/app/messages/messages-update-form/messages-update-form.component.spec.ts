import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MessagesUpdateFormComponent } from './messages-update-form.component';

describe('MessagesUpdateFormComponent', () => {
  let component: MessagesUpdateFormComponent;
  let fixture: ComponentFixture<MessagesUpdateFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MessagesUpdateFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MessagesUpdateFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
