import { Injectable } from '@angular/core';
import {Http, Headers} from '@angular/http';
import {HttpParams} from '@angular/common/http';

@Injectable()
export class MessagesService {
  http:Http; //http is the name, Http is the type
  getMessages(){
   //return ['Message1', 'Message2', 'Message3', 'message4'];
   //get messages from the SLIM rest API (dont say DB)
   return this.http.get('http://localhost:10080/slim/messages'); 

  }

  getMessage(id) {
    return this.http.get('http://localhost:10080/slim/messages/' + id);
  }


  postMessage(data){
    let options = {
      headers:new Headers({
        'content-type':'application/x-www-form-urlencoded'
      })
    }
    let params = new HttpParams().append('message', data.message).append('user_id', data.user);
    return this.http.post('http://localhost:10080/slim/messages', params.toString(), options);
    
  }

  putMessage(data,key){
    let options = {
      headers: new Headers({
        'content-type':'application/x-www-form-urlencoded'
      })
    }
    let params = new HttpParams().append('message',data.message);
    return this.http.put('http://localhost:10080/slim/messages/'+ key,params.toString(), options);
  }

  deleteMessage(key){
    return this.http.delete('http://localhost:10080/slim/messages/'+key);
  }

  constructor(http:Http) { //http is a new object of type Http
    this.http = http;
  }

}